package Zlatopolskiy.task10_44;

/**
 * Created by Malyutin on 01.04.2017.
 */
public class Task10_44 {
    public static void main(String[] args) {
        int a = 234;
        System.out.println(root(a));

    }
    public static int root (int a) {
        if (sum(a) / 10 > 0)
            return root(sum(a));
        else
            return sum(a);
    }

    public static int sum(int a){
        if (a > 0)
            return a%10+sum(a/10);
        else
            return  0;
    }
}
