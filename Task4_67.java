package Zlatopolskiy.task4_67;

import java.util.Scanner;

/**
 * Created by Malyutin on 29.03.2017.
 */
public class Task4_67 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextByte();
        if (k % 7 > 0 && k % 7 < 6 )
            System.out.println("It's workday");
        else
            System.out.println("It's weekend");
    }
}
