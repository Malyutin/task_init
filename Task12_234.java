package Zlatopolskiy.task12_234;

/**
 * Created by Malyutin on 07.04.2017.
 */
public class Task12_234 {
    public static void main(String[] args) {
        int n = 5;
        int[][] a = new int[][]{{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15},{16,17,18,19,20},{21,22,23,24,25}};
        int k = 2;
        int s = 1;
        int temp = 0;
        for (int i = k; i < n ; i++) {
            for (int j = 0; j < n ; j++) {
                if(i < n - 1) {
                    temp = a[i+1][j];
                    a[i][j] = temp;
                    a[i+1][j] = 0;
                }
                else
                    a[i][j] = 0;
            }
        }

        for (int i = 0; i < n ; i++) {
            for (int j = s; j < n ; j++) {
                if(j < n - 1) {
                    temp = a[i][j+1];
                    a[i][j] = temp;
                    a[i][j+1] = 0;
                }
                else
                    a[i][j] = 0;
            }
        }

        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n ; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
