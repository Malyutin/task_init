package Zlatopolskiy.task6_8;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Malyutin on 30.03.2017.
 */
public class Task6_8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> array = new ArrayList<>();
        System.out.println("Vvedite n:");
        int n = scanner.nextInt();
        for (int i = 0 ; i < n ; i++) {
            array.add((int) Math.pow(i+1,2));
            if (array.get(i) > n)
                break;
            System.out.println(array.get(i));

        }
    }
}
