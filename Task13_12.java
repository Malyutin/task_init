package Zlatopolskiy.task13_12;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Malyutin on 07.04.2017.
 */
public class Task13_12 {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Worker> workers = new ArrayList<>();
        Date today = new Date();
        SimpleDateFormat monthformat = new SimpleDateFormat();
        monthformat.applyPattern("MM");
        int todayMonth = Integer.parseInt(monthformat.format(today));
        SimpleDateFormat yearformat = new SimpleDateFormat();
        yearformat.applyPattern("yyyy");
        int todayYear = Integer.parseInt(yearformat.format(today));
        for (int i = 0; i < 20 ; i++) {
            workers.add(new Worker());
        }
        for (Worker worker : workers) {
            if ((todayYear - worker.getYear() > 3) || (todayYear - worker.getYear() == 3) && (todayMonth - worker.getMonth() >= 0)){
                System.out.println(worker.getLastName() + " " + worker.getFirstName() + " " + worker.getPatronymic() + " " + worker.getAddress());
            }
        }
    }
}
class Worker {
    private String firstName;
    private String lastName;
    private String patronymic;
    private String address;
    private Date enterDate;
    public Worker(String firstName, String lastName, String patronymic, String address, String date) throws ParseException {
        setFirstName(firstName);
        setLastName(lastName);
        setPatronymic(patronymic);
        setAddress(address);
        setEnterDate(date);
    }
    public Worker() throws ParseException, IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите имя:");
        setFirstName(reader.readLine());
        System.out.println("Введите фамилию:");
        setLastName(reader.readLine());
        System.out.println("Введите отчество:");
        setPatronymic(reader.readLine());
        System.out.println("Введите адрес:");
        setAddress(reader.readLine());
        System.out.println("Введите дату начала работы:");
        setEnterDate(reader.readLine());
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setPatronymic(String patrinymic) {
        this.patronymic = patrinymic;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setEnterDate(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        this.enterDate = format.parse(date);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public String getDate() {
        SimpleDateFormat myformat = new SimpleDateFormat();
        myformat.applyPattern("dd.MM.yyyy");
        return myformat.format(enterDate);
    }

    public int getMonth() {
        SimpleDateFormat myformat = new SimpleDateFormat();
        myformat.applyPattern("MM");
        return Integer.parseInt(myformat.format(enterDate));
    }

    public int getYear() {
        SimpleDateFormat myformat = new SimpleDateFormat();
        myformat.applyPattern("yyyy");
        return Integer.parseInt(myformat.format(enterDate));
    }
}

