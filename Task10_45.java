package Zlatopolskiy.task10_45;

/**
 * Created by Malyutin on 01.04.2017.
 */
public class Task10_45 {
    public static void main(String[] args) {
        int a1 = 1;
        int d = 2;
        int n = 3;
        System.out.println(arifmSum(a1,d,n));
        System.out.println(arifmN(a1,d,n));

    }
    public static int arifmSum (int a1, int d, int n) {
        if (n > 0)
            return a1 + arifmSum(a1+d,d,n-1);
        else
            return 0;
    }
   public static int arifmN (int a1, int d, int n) {
        if (n == 1)
            return  a1;
        else
            return arifmN(a1,d,n-1)+d;

   }
}
