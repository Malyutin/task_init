package Zlatopolskiy.task10_49;

/**
 * Created by Malyutin on 03.04.2017.
 */
public class Task10_49 {
    public static void main(String[] args) {
        int n = 5;
        int[] ar = new int[]{230, 125, 112, 14, 247};
        System.out.println(maxInd(ar,n-1));

    }
    public static int maxInd(int[] array, int n){
        if (n > 0){
            if (array[n] > array[maxInd(array,n-1)]) {
                return n;
            }
            else {
                return maxInd(array,n-1);
            }
        }
        else
            return 0;

    }
}
