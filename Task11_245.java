package Zlatopolskiy.task11_245;

/**
 * Created by Malyutin on 05.04.2017.
 */
public class Task11_245 {
    public static void main(String[] args) {
        int[] sourceArray = new int[]{2, 3, -2, -6, 5, -9, 8, 1, -3, 2};
        int[] newArray = new int[sourceArray.length];
        int j = 0;
        int k = newArray.length - 1;
        for (int i = 0; i < sourceArray.length; i++) {
            if (sourceArray[i] < 0) {
                newArray[j] = sourceArray[i];
                j++;
            } else {
                newArray[k] = sourceArray[i];
                k--;
            }

        }
        for (int x : newArray) {
            System.out.print(x + " ");
        }
    }
}

