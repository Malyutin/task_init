package Zlatopolskiy.task10_46;

/**
 * Created by Malyutin on 02.04.2017.
 */
public class Task10_46 {
    public static void main(String[] args) {
        int b1 = 1;
        int q = 2;
        int n = 10;
        System.out.println(geomSum(b1,q,n));
        System.out.println(geomN(b1,q,n));

    }
    public static int geomSum (int b1, int q, int n) {
        if (n > 0)
            return b1 + geomSum(b1*q,q,n-1);
        else
            return 0;
    }
    public static int geomN (int b1, int q, int n) {
        if (n == 1)
            return  b1;
        else
            return geomN(b1,q,n-1)*q;

    }
}
