package Zlatopolskiy.task10_51;

/**
 * Created by Malyutin on 04.04.2017.
 */
public class task10_51 {
    public static void main(String[] args) {
        procedure1(5);
        procedure2(5);
        procedure3(5);

    }
    public static void procedure1 (int n){
        if (n > 0){
            System.out.println(n);
            procedure1(n - 1);
        }
    }

    public static void procedure2 (int n){
        if (n > 0){
            procedure2(n - 1);
            System.out.println(n);
        }
    }

    public static void procedure3(int n){
        if (n > 0){
            System.out.println(n);
            procedure3(n-1);
            System.out.println(n);
        }
    }
}
