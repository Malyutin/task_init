package Zlatopolskiy.task10_42;

/**
 * Created by Malyutin on 31.03.2017.
 */
public class Task10_42 {
    public static void main(String[] args) {
        double a = 2;
        int n = 10;
        System.out.println(pow(a,n));
    }
    public static double pow (double a, int n){
        if (n>0)
        return a*pow(a,n-1);
        else
            return 1;
    }
}
