package Zlatopolskiy.task10_43;

/**
 * Created by Malyutin on 01.04.2017.
 */
public class Task10_43 {
    public static void main(String[] args) {
        int a = 23456;
        System.out.println("Сумма цифр числа: " + sum(a));
        System.out.println("Кол-во цифр числа: " + count(a));


    }

    public static int sum(int a){
        if (a > 0)
            return a%10+sum(a/10);
        else
            return  0;
    }
    public static int count (int a) {
        if (a > 0)
            return 1 + count(a/10);
        else
            return 0;
    }
}
