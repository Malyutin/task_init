package Zlatopolskiy.task10_47;

/**
 * Created by Malyutin on 02.04.2017.
 */
public class Task10_47 {
    public static void main(String[] args) {
        int k = 10;
        System.out.println(fib(k));

    }
    public static int fib (int k) {
        if (k > 2)
            return fib(k-1) + fib(k-2);
        else
            return 1;
    }
}
