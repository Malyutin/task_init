package Zlatopolskiy.task9_17;

import java.util.Scanner;

/**
 * Created by Malyutin on 31.03.2017.
 */
public class Task9_17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vvedite slovo:");
        String word = scanner.nextLine();
        char startChar = word.charAt(0);
        char endChar = word.charAt(word.length()-1);
        if (startChar == endChar)
            System.out.println("Verno, na odnu bukvu");
        else
            System.out.println("Neverno, na raznye bukvy");



    }
}
