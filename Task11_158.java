package Zlatopolskiy.task11_158;

/**
 * Created by Malyutin on 05.04.2017.
 */
public class Task11_158 {
    public static void main(String[] args) {
        int[] array = new int[]{1,3,6,3,5,7,4,3,9,7};
        for (int i = 0; i < array.length ; i++) {
            int a = array[i];
            for (int j = i + 1; j < array.length ; j++) {
                if (a == array[j]) {
                    for (int k = j + 1; k < array.length; k++) {
                        array[k - 1] = array[k];
                        array[k] = 0;

                    }
                }
            }
        }
        for (int s: array) {
            System.out.print(s + " ");
        }
    }
}
