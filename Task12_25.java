package Zlatopolskiy.task12_25;

/**
 * Created by Malyutin on 05.04.2017.
 */
public class Task12_25 {
    public static void main(String[] args) {
        taskA();
        System.out.println();
        taskB();
        System.out.println();
        taskV();
        System.out.println();
        taskG();
        System.out.println();
        taskD();
        System.out.println();
        taskE();
        System.out.println();
        taskJ();
        System.out.println();
        taskZ();
        System.out.println();
        taskI();
        System.out.println();
        taskK();
        System.out.println();
        taskL();
        System.out.println();
        taskM();
        System.out.println();
        taskN();
        System.out.println();
        taskO();
        System.out.println();
        taskP();
        System.out.println();
        taskR();

    }
    public static void taskA(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 0; i < 12 ; i++) {
            for (int j = 0; j < 10 ; j++) {
                a[i][j] = s++;
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }
    public static void taskB(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 0; i < 10  ; i++) {
            for (int j = 0; j < 12 ; j++) {
                a[j][i] = s++;
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskV(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 0; i < 12  ; i++) {
            for (int j = 9; j >= 0 ; j--) {
                a[i][j] = s++;
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskG(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 0 ; i < 10  ; i++) {
            for (int j = 11; j >= 0 ; j--) {
                a[j][i] = s++;
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskD(){
        int[][] a = new int[10][12];
        int s = 1;
        for (int i = 0 ; i < 10  ; i++) {
            if (i%2 == 0) {
                for (int j = 0; j < 12; j++) {
                    a[i][j] = s++;
                }
            }
            else{
                for (int j = 11; j >= 0; j--) {
                    a[i][j] = s++;
                }
            }
        }
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 12 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskE(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 0 ; i < 10  ; i++) {
            if (i%2 == 0) {
                for (int j = 0; j < 12; j++) {
                    a[j][i] = s++;
                }
            }
            else{
                for (int j = 11; j >= 0; j--) {
                    a[j][i] = s++;
                }
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskJ(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 11; i >= 0  ; i--) {
            for (int j = 0; j < 10 ; j++) {
                a[i][j] = s++;
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskZ(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 9; i >= 0  ; i--) {
            for (int j = 0; j < 12 ; j++) {
                a[j][i] = s++;
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskI(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 11; i >= 0  ; i--) {
            for (int j = 9; j >= 0 ; j--) {
                a[i][j] = s++;
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskK(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 9; i >= 0  ; i--) {
            for (int j = 11; j >= 0 ; j--) {
                a[j][i] = s++;
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskL(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 11; i >= 0  ; i--) {
            if (i%2 != 0) {
                for (int j = 0; j < 10; j++) {
                    a[i][j] = s++;
                }
            }
            else{
                for (int j = 9; j >= 0; j--) {
                    a[i][j] = s++;
                }
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskM(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 0; i < 12  ; i++) {
            if (i%2 != 0) {
                for (int j = 0; j < 10; j++) {
                    a[i][j] = s++;
                }
            }
            else{
                for (int j = 9; j >= 0; j--) {
                    a[i][j] = s++;
                }
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskN(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 9; i >= 0  ; i--) {
            if (i%2 != 0) {
                for (int j = 0; j < 12; j++) {
                    a[j][i] = s++;
                }
            }
            else{
                for (int j = 11; j >= 0; j--) {
                    a[j][i] = s++;
                }
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskO(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 0; i < 10  ; i++) {
            if (i%2 != 0) {
                for (int j = 0; j < 12; j++) {
                    a[j][i] = s++;
                }
            }
            else{
                for (int j = 11; j >= 0; j--) {
                    a[j][i] = s++;
                }
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskP(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 11; i >= 0  ; i--) {
            if (i%2 == 0) {
                for (int j = 0; j < 10; j++) {
                    a[i][j] = s++;
                }
            }
            else{
                for (int j = 9; j >= 0; j--) {
                    a[i][j] = s++;
                }
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

    public static void taskR(){
        int[][] a = new int[12][10];
        int s = 1;
        for (int i = 9; i >= 0  ; i--) {
            if (i%2 == 0) {
                for (int j = 0; j < 12; j++) {
                    a[j][i] = s++;
                }
            }
            else{
                for (int j = 11; j >= 0; j--) {
                    a[j][i] = s++;
                }
            }
        }
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10 ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }
    }

}
