package Zlatopolskiy.task9_42;

/**
 * Created by Malyutin on 31.03.2017.
 */
public class Task9_42 {
    public static void main(String[] args) {
        String word = "Slovo";
        StringBuilder inversion = new StringBuilder();
        for (int i = word.length()-1 ; i >= 0 ; i--) {
            inversion.append(word.charAt(i));
        }
        System.out.println(inversion.toString());
    }
}
