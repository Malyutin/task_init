package Zlatopolskiy.task10_52;

/**
 * Created by Malyutin on 04.04.2017.
 */
public class Task10_52 {
    public static void main(String[] args) {
        reverse(12345);

    }
    public static void reverse(int n){
        if (n > 0){
            System.out.print(n%10);
            reverse(n/10);
        }
    }
}
