package Zlatopolskiy.task4_115;

import java.util.Scanner;

/**
 * Created by Malyutin on 30.03.2017.
 */
public class Task4_115a {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int numInCycle = (n - 1984) % 60;
        int animal = 1 + numInCycle % 12;
        int color = 1 + numInCycle % 10;
        String animalName="";
        String animalColor="";
        switch (color) {
            case 1:
                animalColor = "Зеленый";
                break;
            case 2:
                animalColor = "Зеленый";
                break;
            case 3:
                animalColor = "Красный";
                break;
            case 4:
                animalColor = "Красный";
                break;
            case 5:
                animalColor = "Желтый";
                break;
            case 6:
                animalColor = "Желтый";
                break;
            case 7:
                animalColor = "Белый";
                break;
            case 8:
                animalColor = "Белый";
                break;
            case 9:
                animalColor = "Черный";
                break;
            case 10:
                animalColor = "Черный";
                break;
            default:animalColor = "Нет цвета";
        }
        switch (animal) {
            case 1:
                animalName = "Крыса";
                break;
            case 2:
                animalName = "Корова";
                break;
            case 3:
                animalName =  "Тигр";
                break;
            case 4:
                animalName = "Заяц";
                break;
            case 5:
                animalName = "Дракон";
                break;
            case 6:
                animalName = "Змея";
                break;
            case 7:
                animalName = "Лошадь";
                break;
            case 8:
                animalName = "Овца";
                break;
            case 9:
                animalName = "Обезьяна";
                break;
            case 10:
                animalName = "Петух";
                break;
            case 11:
                animalName = "Собака";
                break;
            case 12:
                animalName = "Свинья";
                break;
            default: animalName = "Нет животного";
        }
        System.out.println(animalName + ", " + animalColor);
    }
}
