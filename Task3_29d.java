package Zlatopolskiy.task3_29;

import java.util.Scanner;

/**
 * д) только одно из чисел X, Y и Z кратно пяти;
 */
public class Task3_29d {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int iX = scanner.nextInt();
        int iY = scanner.nextInt();
        int iZ = scanner.nextInt();
        boolean x = iX % 5 == 0;
        boolean y = iY % 5 == 0;
        boolean z = iZ % 5 == 0;
        if (!x & !y & z | !x & y & !z | x & !y & !z)
            System.out.println(true);
        else
            System.out.println(false);
    }

}
