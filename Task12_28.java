package Zlatopolskiy.task12_28;

import java.util.Scanner;

/**
 * Created by Malyutin on 06.04.2017.
 */
public class Task12_28 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vvedite N:");
        int N = scanner.nextInt();
        int s = 1;
        int[][] a = new int[N][N];

        for (int v = 0; v < N/2+1 ; v++) {
            for (int i = v; i < N - v; i++) {
                a[v][i] = s++;
            }
            for (int i = v + 1; i < N - v  ; i++) {
                a[i][N-v-1] = s++;
            }
            for (int i = N - v - 2; i >= v ; i--) {
                a[N-v-1][i] = s++;
            }
            for (int i = N - v - 2; i > v ; i--) {
                a[i][v] = s++;
            }

        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N ; j++) {
                System.out.print(a[i][j] + " ");

            }
            System.out.println();
        }

    }

}
