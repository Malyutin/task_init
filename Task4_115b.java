package Zlatopolskiy.task4_115;

import java.util.Scanner;

/**
 * Created by Malyutin on 30.03.2017.
 */
public class Task4_115b {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int numInCycle = n % 60;
        int animal = 1 + numInCycle % 12;
        int color = 1 + numInCycle % 10;
        String animalName="";
        String animalColor="";
        switch (color) {
            case 1:
                animalColor = "Белый";
                break;
            case 2:
                animalColor = "Белый";
                break;
            case 3:
                animalColor = "Черный";
                break;
            case 4:
                animalColor = "Черный";
                break;
            case 5:
                animalColor = "Зеленый";
                break;
            case 6:
                animalColor = "Зеленый";
                break;
            case 7:
                animalColor = "Красный";
                break;
            case 8:
                animalColor = "Красный";
                break;
            case 9:
                animalColor = "Желтый";
                break;
            case 10:
                animalColor = "Желтый";
                break;
            default:animalColor = "Нет цвета";
        }
        switch (animal) {
            case 1:
                animalName = "Обезьяна";
                break;
            case 2:
                animalName = "Петух";
                break;
            case 3:
                animalName =  "Собака";
                break;
            case 4:
                animalName = "Свинья";
                break;
            case 5:
                animalName = "Крыса";
                break;
            case 6:
                animalName = "Корова";
                break;
            case 7:
                animalName = "Тигр";
                break;
            case 8:
                animalName = "Заяц";
                break;
            case 9:
                animalName = "Дракон";
                break;
            case 10:
                animalName = "Змея";
                break;
            case 11:
                animalName = "Лошадь";
                break;
            case 12:
                animalName = "Овца";
                break;
            default: animalName = "Нет животного";
        }
        System.out.println(animalName + ", " + animalColor);
    }
}
