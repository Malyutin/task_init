package Zlatopolskiy.task12_63;

/**
 * Created by Malyutin on 06.04.2017.
 */
public class Task12_63 {
    public static void main(String[] args) {
        int[][] a = new int[][]{{21,34,30,23},
                                {23,20,18,26},
                                {24,27,19,21},
                                {24,25,34,33},
                                {20,22,15,21},
                                {27,24,29,22},
                                {23,23,15,24},
                                {23,21,27,31},
                                {23,29,20,34},
                                {23,23,22,16},
                                {23,22,17,19}};

        for (int i = 0; i < 11 ; i++) {
            int parallel = 0;
            for (int j = 0; j < 4 ; j++) {
                parallel += a[i][j];

            }
            System.out.println("В параллели " + (i+1) + " среднее кол-во учеников: " + parallel/4);

        }
    }
}
