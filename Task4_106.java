package Zlatopolskiy.task4_106;

import java.util.Scanner;

/**
 * Created by Malyutin on 30.03.2017.
 */
public class Task4_106 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int month = scanner.nextInt();
        switch (month) {
            case 12:
            case 1:
            case 2:
                System.out.println("Winter");
                break;
            case 3:
            case 4:
            case 5:
                System.out.println("Spring");
                break;
            case 6:
            case 7:
            case 8:
                System.out.println("Summer");
                break;
            case 9:
            case 10:
            case 11:
                System.out.println("Autumn");
                break;
                default:
                    System.out.println("Unknown");



        }
    }
}
