package Zlatopolskiy.task9_185;

/**
 * Created by Malyutin on 31.03.2017.
 */
public class Task9_185 {
    public static void main(String[] args) {
        String expression = "(a*x-b())9)*(c*((a-d/c))";
        int countOpen = 0;
        int countClose = 0;
        for (int i = 0; i < expression.length() ; i++) {
            if (expression.charAt(i) == '(')
                countOpen++;
            if (expression.charAt(i) == ')')
                countClose++;
            if (countClose > countOpen) {
                System.out.println("Имеется лишняя закрывающаяся скобка на позиции " + i);
                break;
            }
        }
        if (countClose < countOpen)
            System.out.println("Имеются лишние открывающиеся скобки в количестве: " + (countOpen - countClose));

    }



}
