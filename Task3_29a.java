package Zlatopolskiy.task3_29;

import java.util.Scanner;

/**
 * а) каждое из чисел X и Y нечетное;
 */
public class Task3_29a {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int X = scanner.nextInt();
        int Y = scanner.nextInt();
        if (X % 2 != 0 & Y % 2 != 0 )
            System.out.println(true);
        else
            System.out.println(false);

    }

}
