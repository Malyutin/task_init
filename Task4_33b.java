package Zlatopolskiy.task4_33;

import java.util.Scanner;

/**
 * Created by Malyutin on 29.03.2017.
 */
public class Task4_33b {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vvedite chislo");
        int num = scanner.nextInt();
        int lastNum = num % 10;
        if ( lastNum % 2 != 0)
            System.out.println("Verno, nechetnoi cyfroi");
    }
}
