package Zlatopolskiy.task3_29;

import java.util.Scanner;

/**
 * в) хотя бы одно из чисел X и Y равно нулю;
 */
public class Task3_29v {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int X = scanner.nextInt();
        int Y = scanner.nextInt();
        if (X == 0 | Y == 0)
            System.out.println(true);
        else
            System.out.println(false);

    }

}
