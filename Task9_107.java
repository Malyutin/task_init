package Zlatopolskiy.task9_107;

/**
 * Created by Malyutin on 31.03.2017.
 */
public class Task9_107 {
    public static void main(String[] args) {
        String word = "офтальмология";
        StringBuilder builder = new StringBuilder(word);
        int ai;
        int oi;
        if (word.contains("а") && word.contains("о")) {
            ai = builder.indexOf("а");
            oi = builder.lastIndexOf("о");
            builder.deleteCharAt(oi);
            builder.insert(oi, word.charAt(ai));
            builder.deleteCharAt(ai);
            builder.insert(ai, word.charAt(oi));
        }
        System.out.println(builder.toString());
    }
}
