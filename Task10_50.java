package Zlatopolskiy.task10_50;

/**
 * Created by Malyutin on 03.04.2017.
 */
public class Task10_50 {
    public static void main(String[] args) {
        System.out.println(akker(3, 5));
    }

    public static int akker(int n, int m) {
        if (n == 0)
            return m+1;
        else {
            if (m == 0)
                return akker(n - 1, 1);
            else
                return akker(n-1,akker(n,m-1));
        }
    }


}
