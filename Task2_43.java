package Zlatopolskiy.task2_43;

import java.util.Scanner;

/**
 * Created by Malyutin on 28.03.2017.
 */
public class Task2_43 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число a");
        int a = scanner.nextInt();
        System.out.println("Введите число b");
        int b = scanner.nextInt();
        int test1 = a % b;
        int test2 = b % a;
        System.out.println(test1*test2 + 1);
    }
}
