package Zlatopolskiy.task1_17;

import java.util.Scanner;

/**
 * Created by Malyutin on 28.03.2017.
 */
public class Task1_17 {
    public static void main(String[] args) {

    }

    public static class  Task1_17_o {
        private double x;
        public void setX(double x){
            this.x = x;
        }
        public double calculate(){
            double result = Math.sqrt(1 - Math.pow(Math.sin(x), 2));
            return result;
        }
    }

    public static class  Task1_17_p {
        private double x;
        public void setX(double x){
            this.x = x;
        }
        public double calculate(double a, double b, double c){
            double result = Math.pow(Math.sqrt(a*Math.pow(x,2) + b*x + c) , -1);
            return result;
        }
    }

    public static class  Task1_17_r {
        private double x;
        public void setX(double x){
            this.x = x;
        }
        public double calculate(){
            double result = (Math.sqrt(x+1) + Math.sqrt(x-1)) / (2*Math.sqrt(x));
            return result;
        }
    }

    public static class  Task1_17_s {
        private double x;
        public void setX(double x){
            this.x = x;
        }
        public double calculate(){
            double result = Math.abs(x) + Math.abs(x+1);
            return result;
        }
    }
}
