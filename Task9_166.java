package Zlatopolskiy.task9_166;

import java.util.ArrayList;

/**
 * Created by Malyutin on 31.03.2017.
 */
public class Task9_166 {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        String sentence = "Мама мыла раму долго и упорно";
        for (String s : sentence.split(" ")) {
            words.add(s);
        }
        String temp = words.get(0);
        words.set(0,words.get(words.size()-1));
        words.set(words.size()-1,temp);
        StringBuilder builder = new StringBuilder();
        for (String s: words) {
            builder.append(s+" ");
        }
        System.out.println(builder.toString().trim());

    }
}
