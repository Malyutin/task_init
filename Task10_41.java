package Zlatopolskiy.task10_41;

/**
 * Created by Malyutin on 31.03.2017.
 */
public class Task10_41 {
    public static void main(String[] args) {
        int n = 10;
        System.out.println(factorial(n));

    }
    public static int factorial(int n){
        if (n>0)
            return n*factorial(n-1);
        else
            return 1;
    }
}
