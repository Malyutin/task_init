package Zlatopolskiy.task10_56;

import java.util.Scanner;

/**
 * Created by Malyutin on 04.04.2017.
 */
public class Task10_56 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vvedite chislo: ");
        int number = scanner.nextInt();
        int numOfChecks=number/2;
        if (primeTest(number,numOfChecks) == true)
            System.out.println("простое");
        else
            System.out.println("не простое");
    }
    public static boolean primeTest(int a, int i){
        if (i > 1){
            if (a%i > 0){
                return primeTest(a,i-1) ;
            }
            else
                return false;
        }
        else
            return true;
    }
}
