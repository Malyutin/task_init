package Zlatopolskiy.task9_15;

import java.util.Scanner;

/**
 * Created by Malyutin on 31.03.2017.
 */
public class Task9_15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vvedite slovo:");
        String word = scanner.nextLine();
        System.out.println("Vvedite chislo k:");
        int k = scanner.nextInt();
        System.out.println(word.charAt(k));
    }
}
