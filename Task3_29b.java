package Zlatopolskiy.task3_29;

import java.util.Scanner;

/**
 * б) только одно из чисел X и Y меньше 20;
 */
public class Task3_29b {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int X = scanner.nextInt();
        int Y = scanner.nextInt();
        if (X < 20 ^ Y < 20)
            System.out.println(true);
        else
            System.out.println(false);

    }

}
