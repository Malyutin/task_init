package Zlatopolskiy.task5_64;

import java.util.Scanner;

/**
 * Created by Malyutin on 30.03.2017.
 */
public class Task5_64 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] humans = new int[12];
        int[] squares = new int[12];
        int density = 0;
        for (int i = 0; i < 12 ; i++) {
            System.out.println("Введите кол-во жителей следующего района:");
            humans[i] = scanner.nextInt();
            System.out.println("Введите площадь следующего района, км2:");
            squares[i] = scanner.nextInt();
            density += humans[i]/squares[i];
        }
        System.out.println("Средняя плотность населения " + density/12 + " человека на км2");

    }
}
