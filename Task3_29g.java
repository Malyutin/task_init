package Zlatopolskiy.task3_29;

import java.util.Scanner;

/**
 * г) каждое из чисел X, Y, Z отрицательное;
 */
public class Task3_29g {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int X = scanner.nextInt();
        int Y = scanner.nextInt();
        int Z = scanner.nextInt();
        if (X < 0 & Y < 0 & Z < 0)
            System.out.println(true);
        else
            System.out.println(false);

    }

}
