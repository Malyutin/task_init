package Zlatopolskiy.task2_31;

import java.util.Scanner;

/**
 * Created by Malyutin on 28.03.2017.
 */
public class Task2_31 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число n");
        int n = scanner.nextInt();
        int x = 0;
        int secondNum = n % 10;
        int tempNum = n / 10;
        x = (tempNum / 10) * 100 + secondNum * 10 + tempNum % 10;
        System.out.println("Искомое число " + x);
    }
}
