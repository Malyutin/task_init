package Zlatopolskiy.task6_87;

import java.util.Scanner;

/**
 * Created by Malyutin on 31.03.2017.
 */
class Task8_87 {
    public static void main(String[] args) {
        Game.play();
    }
}
class Game{
    public static int teamToScore;

    public static void play() {
        Scanner scanner = new Scanner(System.in);
        Team team1 = new Team();
        Team team2 = new Team();
        while (true) {

            if (team1.getTeamName().equals("")) {
                System.out.println("Enter team #1:");
                team1.setTeamName(scanner.nextLine());
            }
            if (team2.getTeamName().equals("")) {
                System.out.println("Enter team #2:");
                team2.setTeamName(scanner.nextLine());
            }
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            teamToScore = scanner.nextInt();
            switch (teamToScore) {
                case 1:
                    System.out.println("Enter score (1 or 2 or 3):");
                    team1.setTeamScore(scanner.nextInt());
                    System.out.println(score(team1, team2));
                    break;
                case 2:
                    System.out.println("Enter score (1 or 2 or 3):");
                    team2.setTeamScore(scanner.nextInt());
                    System.out.println(score(team1, team2));
                    break;
                case 0:
                    System.out.println(result(team1, team2));
                    break;
                default:
                    break;
            }
            if (teamToScore == 0)
                break;
        }
    }
    public static String score(Team team1, Team team2){
        String intermediateScore;
        intermediateScore = "счет: " + team1.getTeamScore() + " : " + team2.getTeamScore();
        return intermediateScore;
    }
    public static String result(Team team1, Team team2){
        String result;
        if (team1.getTeamScore() > team2.getTeamScore())
            result = "Игра закончена. Победила команда " + team1.getTeamName() + ", " + score(team1,team2);
        else
            if (team1.getTeamScore() < team2.getTeamScore())
                result = "Игра закончена. Победила команда " + team2.getTeamName() + ", " + score(team1,team2);
            else
                result = "Игра закончена. Ничья, " + score(team1,team2);
        return result;
    }
}

class Team{
    private String teamName="";
    private int teamScore;

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamScore(int teamScore) {
        if(teamScore < 0 || teamScore > 3) {
            this.teamScore += 0;
            System.out.println("Так не бывает");
        }
        else
            this.teamScore += teamScore;
    }

    public int getTeamScore() {
        return teamScore;
    }
}


