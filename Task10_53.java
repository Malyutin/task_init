package Zlatopolskiy.task10_53;

import java.util.Scanner;

/**
 * Created by Malyutin on 04.04.2017.
 */
public class Task10_53 {
    public static void main(String[] args) {
        int[] a = new int[]{4,5,3,9,10,34,21,3,7};
        reverseArray(a,a.length-1);

    }
    public static void reverseArray(int[] a, int n){
        if (n >= 0 ){
            System.out.println(a[n]);
            reverseArray(a,n-1);

        }
    }
}
