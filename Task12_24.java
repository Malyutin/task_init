package Zlatopolskiy.task12_24;

import java.util.Scanner;

/**
 * Created by Malyutin on 05.04.2017.
 */
public class Task12_24 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vvedite N:");
        int N = scanner.nextInt();
        taskA(N);
        taskB(N);
    }
    public static void taskA(int N){
        int[][] arrray = new int[N][N];
        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                if (i == 0 || j ==0)
                    arrray[i][j] = 1;
                else
                    arrray[i][j] = arrray[i-1][j] + arrray[i][j-1];
            }

        }
        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                System.out.print(arrray[i][j] + " ");
            }
            System.out.println();

        }
    }
    public static void taskB(int N){
        int[][] arrray = new int[N][N];
        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                    arrray[i][j] = j + 1;
                    if (i>0)
                        arrray[i][j] = (arrray[i][j]+i - 1)%N +1;
             }

        }
        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                System.out.print(arrray[i][j] + " ");
            }
            System.out.println();

        }
    }
}
