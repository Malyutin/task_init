package Zlatopolskiy.task10_48;

/**
 * Created by Malyutin on 02.04.2017.
 */
public class Task10_48 {
    public static void main(String[] args) {
        int n = 5;
        int[] ar = new int[]{23, 5, 112, 14, 27};
        System.out.println(maxElem(ar,n-1));

    }
    public static int maxElem(int[] array, int n){
        if (n > 0){
            if (array[n] > maxElem(array,n-1)) {
                return array[n];
            }
            else {
                return maxElem(array,n-1);
            }
        }
        else
            return array[0];

    }
}
