package Zlatopolskiy.task4_15;

import java.util.Scanner;

/**
 * Created by Malyutin on 29.03.2017.
 */
public class Task4_15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите месяц рождения");
        int bdMonth = scanner.nextInt();
        System.out.println("Введите год рождения");
        int bdYear = scanner.nextInt();
        System.out.println("Введите сегодняшний месяц");
        int tdMonth = scanner.nextInt();
        System.out.println("Введите сегодняшний год");
        int tdYear = scanner.nextInt();
        int totalYears = 0;
        if (bdMonth <= tdMonth)
            totalYears = tdYear - bdYear;
        else
            totalYears = tdYear - bdYear - 1;
        System.out.println("Человеку " + totalYears + " лет");
    }
}
