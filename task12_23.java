package Zlatopolskiy.task12_23;

import java.util.Scanner;

/**
 * Created by Malyutin on 05.04.2017.
 */
public class task12_23 {
    public static void main(String[] args) {
        taskA();
        taskB();
        taskC();

    }
    public static void taskA(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vvedite N: ");
        int N = scanner.nextInt();
        int [][] array = new int[N][N];
        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                if (i == j || i == N - 1 - j)
                    array[i][j] = 1;
                else
                    array[i][j] = 0;
            }

        }

        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                System.out.print(array[i][j]);

            }
            System.out.println();

        }
    }

    public static void taskB(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vvedite N: ");
        int N = scanner.nextInt();
        int [][] array = new int[N][N];
        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                if (i == j || i == N - 1 - j || i == N/2 || j == N/2)
                    array[i][j] = 1;
                else
                    array[i][j] = 0;
            }

        }

        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                System.out.print(array[i][j]);

            }
            System.out.println();

        }
    }

    public static void taskC(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vvedite N: ");
        int N = scanner.nextInt();
        int [][] array = new int[N][N];
        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                if (i < N/2 && j >= i && j <= N - 1 - i || i > N/2 && j <= i && j >= N - 1 -i || j == i)
                    array[i][j] = 1;
                else
                    array[i][j] = 0;
            }

        }

        for (int i = 0; i < N ; i++) {
            for (int j = 0; j < N ; j++) {
                System.out.print(array[i][j]);

            }
            System.out.println();

        }
    }
}
