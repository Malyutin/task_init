package Zlatopolskiy.task2_39;

import java.util.Scanner;

/**
 * Created by Malyutin on 28.03.2017.
 */
public class Task2_39 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите часы, h");
        int h = scanner.nextInt();
        System.out.println("Введите минуты, m");
        int m = scanner.nextInt();
        System.out.println("Введите секунды, s");
        int s = scanner.nextInt();
        int degrees = 0;
        int totalSeconds = h*3600 + m*60 + s;
        if (totalSeconds > 43200)
            degrees = (totalSeconds - 43200) / 120;
        else
            degrees = totalSeconds / 120;
        System.out.println(degrees + " градусов");

    }
}
