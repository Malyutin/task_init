package Zlatopolskiy.task2_13;

/**
 * Created by Malyutin on 28.03.2017.
 */
public class Task2_13 {
    public static void main(String[] args) {
        int n = 183;
        double newN=0;
        for (int i = 2; i >= 0 ; i--) {
            newN = newN + (n % 10)*Math.pow(10, i);
            n /= 10;
        }
        System.out.println((int) newN);
    }
}
