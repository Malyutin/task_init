package Zlatopolskiy.task4_36;

import java.util.Scanner;

/**
 * Created by Malyutin on 29.03.2017.
 */
public class Task4_36 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        if (t%5>=0 && t%5<3 || t<3)
            System.out.println("green");
        if (t%5>=3 && t%5<5 || t>=3 && t<5)
            System.out.println("red");
    }
}
